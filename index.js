const Koa = require("koa");
const Router = require("koa-router");
const logger = require("koa-logger");
const bodyParser = require("koa-bodyparser");
const fs = require("fs");
const path = require("path");
const axios = require('axios');
const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV,
})
// const isProd = process.env.NODE_ENV === 'prod';
const Host = 'https://esmeralda.pro';
const api = axios.create({
  baseURL: Host,
  timeout: 14000,
});

const router = new Router();

const GATEWAY_SECRET = 'SDGLSDFJHB278293tuh4g2v9iKn23tg2bhgj9';

router.post('/api/esmeralda/notify', async (ctx) => {
  const { data } = ctx.request.body;
  const secret = ctx.request.headers['x-gateway-secret'];
  if (secret !== GATEWAY_SECRET || !data.touser) {
    ctx.body = {};
    return;
  }
  const result = await cloud.openapi.subscribeMessage.send(data);
  ctx.body = result;
})

// 代理到esmeralda服务器
router.post("/api/esmeralda/gateway", async (ctx) => {
  const { path, data, method } = ctx.request.body;
  const openId = ctx.request.headers["x-wx-openid"];
  if (!openId) {
    ctx.body = {
      code: -1,
      message: 'not allowed',
    }
    return;
  }
  const headers = {
    headers: {
      'x-wx-openid': openId,
      'x-gateway-secret': GATEWAY_SECRET,
    }
  };
  let ret = {};
  if (/get/i.test(method)) {
    ret = await api[method](path, {
      params: data,
      ...headers
    });
  } else {
    ret = await api[method](path, data, {
      ...headers
    });
  }
  ctx.body = ret.data ? ret.data : {code: -1, message: '服务器异常，请稍后再试'};
});

const app = new Koa();
app
  .use(logger())
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods());

const port = process.env.PORT || 80;
async function bootstrap() {
  app.listen(port, () => {
    console.log("启动成功", port);
  });
}
bootstrap();
